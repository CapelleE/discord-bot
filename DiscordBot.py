import discord
import random
import StatusManager
import BTCPrice
import re

btc = BTCPrice.BTCPrice()
client = discord.Client()
sm = StatusManager.StatusManager()

commandPrefix = '\/'
paramDelimiterStart = ', '
paramDelimiterEnd = ''


def login(discordEmail: str, discordPassword: str):
    client.login(discordEmail, discordPassword)
    return client.is_logged_in


def run():
    client.run()


@client.event
def on_ready():
    print('Logged in as: {}'.format(client.user.name))


@client.event
def on_message(message: discord.Message):
    split = message.content.split(' ')
    test = getCommand(message)
    print('Recv message: \"{}\" from {}'.format(message.content, message.author.name))

    if split[0] == '/help':
        showCommands(message)
    if split[0] == '/roll':
        doRoll(message)
    if split[0] == '/rolllist' or split[0] == '/rlist':
        doRollList(message)
    if split[0] == '/btcprice':
        showBTCprice(message)
    if split[0] == '/statusSet' or split[0] == '/status':
        setStatusForUser(message)
    if split[0] == '/statusFor':
        getStatusForUser(message)
    if split[0] == '/statusClear':
        clearStatusForUser(message)


def getCommand(message: discord.Message):
    rePattern = '{}\S+'.format(commandPrefix)
    retVal = re.findall(rePattern, message.content)[0]
    # print('Command = {} | re pattern used = {}'.format(retVal, rePattern))
    return retVal


def getCommandParameters(message: discord.Message, paramDelimStart: str, paramDelimEnd = ''):
    #TODO: Find suitable regex
    pass


def setStatusForUser(message: discord.Message):
    statusText = message.content.replace('/statusSet ', '', 1)
    sm.setStatus(message.author.name, statusText)


def getStatusForUser(message: discord.Message):
    userName = message.content.replace('/statusFor ', '', 1)
    statusText = sm.getStatus(userName)
    client.send_message(message.channel, 'Status for user {}: {}'.format(userName, statusText))


def clearStatusForUser(message: discord.Message):
    username = message.author.name
    sm.clearStatus(username)


def showCommands(message: discord.Message):
    client.send_message(message.channel, ''
                                         'Available commands:\n'
                                         '/help: DUH...\n'
                                         '/roll [min] [max] : Rolls a dice\n'
                                         '/rolllist [words/sentences separated by double space] : Choose a random item in list\n'
                                         '/btcprice: Shows the current BTC price from ticker\n'
                                         '/statusSet | /statusFor | /statusClear\n')


def showBTCprice(message: discord.Message):
    if btc:
        client.send_message(message.channel,
                            "Current BTC price: {} (previous was: {})".format(btc.latest_price, btc.previous_price))


def doRollList(message: discord.Message):
    split = message.content.split('  ')
    split[0] = split[0].replace('/rlist ', '', 1)
    client.send_message(message.channel, '*Random choice: {}*'.format(random.choice(split)))


def doRoll(message: discord.Message):
    split = message.content.split(' ')
    rangeMin = stringToInt(split[1])
    rangeMax = stringToInt(split[2])
    if rangeMin == rangeMax:
        client.send_message(message.channel, "*Your roll: {}* (range {} to {})".format(rangeMin, rangeMin, rangeMax))
        return
    if rangeMin > rangeMax:
        tmp = rangeMax
        rangeMax = rangeMin
        rangeMin = tmp
    roll = random.randrange(rangeMin, rangeMax + 1)
    client.send_message(message.channel, "*Your roll: {}* (range {} to {})".format(roll, rangeMin, rangeMax))


def stringToInt(string: str):
    value = 0
    try:
        value = int(string)
        return value
    except ValueError:
        for ch in string:
            value = value + ord(ch)
            if value >= 97 and value <= 123:
                value = value - 96
        return value


if __name__ == "__main__":
    btc.start()

    if login("capelle.e@gmail.com", "t8EF2PfPea8w") == True:
        run()

    import sys
    sys.stdout.flush()
