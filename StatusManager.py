class StatusManager():
    def __init__(self):
        # Structure:
        # { discordUserInst: 'status',
        #   discordUserInst: 'status', ... }
        self.users = {}

    def setStatus(self, username: str, newStatus: str):
        self.users[username] = newStatus

    def clearStatus(self, username: str):
        if username in self.users:
            del self.users[username]

    def getStatus(self, username: str):
        return self.users.get(username, "Status not set for user {}.".format(username))
