__author__ = 'oscuro'

from threading import Thread
from threading import RLock
import time
import requests


class BTCPrice(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.daemon = False
        self.url = 'https://api.coindesk.com/v1/bpi/currentprice.json'
        self.previous_price = 0.0
        self.latest_price = 0.0
        self.last_updated = ""
        self.running = False
        self.locker = RLock()
        self.fetchPrice()

    def fetchPrice(self):
        r = requests.get(self.url, headers={'Accept': 'application/json'})
        with self.locker:  # save the day!
            self.previous_price = self.latest_price
            self.latest_price = r.json()['bpi']['USD']['rate_float']
            self.last_updated = r.json()['time']['updated']

    def start(self):
        self.running = True
        return super().start()

    def run(self):
        while self.running:
            self.fetchPrice()
            time.sleep(60)

    def stop(self):
        self.running = False
